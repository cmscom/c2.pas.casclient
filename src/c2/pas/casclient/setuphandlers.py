# -*- coding: utf-8 -*-
from Products.CMFPlone.interfaces import INonInstallable
from zope.interface import implementer
from AccessControl.Permissions import manage_users
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PluggableAuthService import registerMultiPlugin
from c2.pas.casclient import plugin


@implementer(INonInstallable)
class HiddenProfiles(object):

    def getNonInstallableProfiles(self):
        """Hide uninstall profile from site-creation and quickinstaller."""
        return [
            'c2.pas.casclient:uninstall',
        ]


def post_install(context):
    """Post install script"""
    # Do something at the end of the installation of this package.


def uninstall(context):
    """Uninstall script"""
    # Do something at the end of the uninstallation of this package.


# def setup_ipbase_plugin(portal):
#     uf = portal.acl_users
#     ids = uf.objectIds()
#
#     if 'c2_pas_casclient' not in ids:
#         factory = uf.manage_addProduct['c2.pas.casclient']
#         factory.manage_add_casclient_helper('c2_pas_casclient', 'CAS Client plugin')
#
#         plugin = uf['c2_pas_casclient']
#         plugin.manage_activateInterfaces(['IExtractionPlugin',
#                                           'IAuthenticationPlugin',
#                                           'IRolesPlugin'])
#
#
# def importVarious(context):
#     site = context.getSite()
#     logger = context.getLogger('c2.pas.casclient')
#
#     setup_ipbase_plugin(site)
#     logger.info('CAS Client PAS plugin imported.')


manage_add_casclient_form = PageTemplateFile('browser/add_plugin',
                            globals(), __name__='manage_add_casclient_form' )


def manage_add_casclient_helper(dispatcher, id, title=None, REQUEST=None):
    """Add an ipbaselogin Helper to the PluggableAuthentication Service."""

    sp = plugin.CasclientHelper(id, title)
    dispatcher._setObject(sp.getId(), sp)

    if REQUEST is not None:
        REQUEST['RESPONSE'].redirect( '%s/manage_workspace'
                                      '?manage_tabs_message='
                                      'casclientHelper+added.'
                                      % dispatcher.absolute_url() )


def register_casclient_plugin():
    try:
        registerMultiPlugin(plugin.CasclientHelper.meta_type)
    except RuntimeError:
        # make refresh users happy
        pass


def register_casclient_plugin_class(context):
    context.registerClass(plugin.CasclientHelper,
                          permission = manage_users,
                          constructors = (manage_add_casclient_form,
                                          manage_add_casclient_helper),
                          visibility = None,
                          icon='browser/icon.gif'
                         )

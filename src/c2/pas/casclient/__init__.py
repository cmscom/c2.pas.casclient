# -*- coding: utf-8 -*-
"""Init and utils."""
from zope.i18nmessageid import MessageFactory
C2PasCasclientMessageFactory = MessageFactory('c2.pas.casclient')

from c2.pas.casclient import setuphandlers


setuphandlers.register_casclient_plugin()


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
    setuphandlers.register_casclient_plugin_class(context)

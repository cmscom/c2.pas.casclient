# -*- coding: utf-8 -*-
from time import time
import socket
import six
from AccessControl.SecurityInfo import ClassSecurityInfo
from Products.PluggableAuthService.plugins.BasePlugin import BasePlugin
from Products.PluggableAuthService.utils import classImplements
from plone.memoize import ram
from zope.interface import alsoProvides
from plone.protect.interfaces import IDisableCSRFProtection

# from App.class_init import default__class_init__ as InitializeClass
try:
    from AccessControl.class_init import InitializeClass
except ImportError:
    from Globals import InitializeClass
from Products.statusmessages.interfaces import IStatusMessage
from logging import getLogger
from xmltodict import parse
from zope.component.hooks import getSite
from zope.component import getUtility
from plone.registry.interfaces import IRegistry
from c2.pas.casclient.controlpanel.interfaces import ICasSettingControlPanel
from c2.pas.casclient.interfaces import ICasclientHelper
from c2.pas.casclient.cas_utils import (
    create_cas_login_url,
    create_cas_logout_url,
    create_cas_validate_url,
)

if six.PY2:
    from urllib2 import urlopen
    from urllib2 import URLError
    from urllib2 import HTTPError
else:
    from urllib.request import urlopen
    from urllib.error import URLError
    from urllib.error import HTTPError


log = getLogger("c2.pas.casclient")


def cache_key(func, *args):
    cache_time = 3600
    chache_time_key = time() // cache_time
    return (chache_time_key, func.__name__)


class CasclientHelper(BasePlugin):
    """Multi plugin for CAS"""

    meta_type = "C2 CAS Client Helper"
    security = ClassSecurityInfo()

    CAS_ASSERTION = "__c2_cas_assertion"
    CAS_REDIRECT_URL = "__c2_cas_redirect_url"

    def __init__(self, id, title=None):
        self._setId(id)
        self.title = title

    # IAuthenticationPlugin
    security.declarePrivate("authenticateCredentials")

    def authenticateCredentials(self, credentials):
        """credentials -> (userid, login)
        o 'credentials' will be a mapping, as returned by IExtractionPlugin.
        o Return a  tuple consisting of user ID (which may be different
          from the login name) and login
        o If the credentials cannot be authenticated, return None.
        """
        if credentials["extractor"] != self.getId():
            return None
        login = credentials["login"]
        return login, login

    # IExtractionPlugin
    security.declarePrivate("extractCredentials")

    def extractCredentials(self, request):
        """request -> {...}
        o Return a mapping of any derived credentials.
        o Return an empty mapping to indicate that the plugin found no
          appropriate credentials.
        """
        credentials = {}

        # Do logout if logout request found
        logoutRequest = request.form.get("logoutRequest", "")  # TODO Check req param
        if logoutRequest:
            self.logout_callback()
            return credentials

        sdm = getattr(self, "session_data_manager", None)  # Zope default session
        assert sdm is not None, "No session data manager found!"

        session = sdm.getSessionData(create=False)
        assertion = self.get_assertion(session)
        if assertion is None:
            # Not already authenticated. Is there a ticket in the URL?
            ticket = request.form.get("ticket", None)
            if not ticket:
                return None  # No CAS authentification
            assertion = self.validate_service_ticket(ticket)
            if assertion is None:
                return None

            # Save current user's session to be used by 'Single Sign Out'
            if not session:
                session = sdm.getSessionData(create=True)

            # TODO 不要だと思う
            # # Get session token as id, it is more reliable
            # session_id = session.getContainerKey()
            # self._sessionStorage.addSession(ticket, session_id)

            # Create a session in the default Plone session factory for username
            # depending on the PLONE version used
            username = assertion.get("user_id")
            self.session._setupSession(
                username, request.response
            )  # Save in cookie by session PAS

            # Save assertion into session
            session = sdm.getSessionData()
            session.set(self.CAS_ASSERTION, assertion)

        username = assertion.get("user_id")
        credentials["login"] = username
        if True:  #  and self.allowedRedirectFromCookie:
            _redirect_url = request.cookies.get(self.CAS_REDIRECT_URL)
            if _redirect_url is not None:
                request.response.expireCookie(self.CAS_REDIRECT_URL)
                # _msg = request.cookies.get(self.CAS_REDIRECT_MSG)
                # if _msg:
                #     messages = IStatusMessage(request)
                #     if not isinstance(_msg, six.text_type):
                #         _msg = _msg.decode('utf-8')
                #     messages.add(_msg, type=u"info")
                #     request.response.expireCookie(self.CAS_REDIRECT_MSG)
                return request.response.redirect(_redirect_url)
        return credentials

    # IChallengePlugin
    security.declarePrivate("challenge")

    def challenge(self, request, response):
        """Assert via the response that credentials will be gathered.
        Takes a REQUEST object and a RESPONSE object.
        Returns True if it fired, False otherwise.
        Two common ways to initiate a challenge:
          - Add a 'WWW-Authenticate' header to the response object.
            NOTE: add, since the HTTP spec specifically allows for
            more than one challenge in a given response.
          - Cause the response object to redirect to another URL (a
            login form page, for instance)
        """
        session = request.SESSION
        try:
            assertion = session[self.CAS_ASSERTION]
            if assertion is not None:
                login = assertion.get("user_id")
            else:
                login = None
        except (LookupError, TypeError):
            login = None
        # Remove current credentials.
        session[self.CAS_ASSERTION] = None
        if True:  # self.allowedRedirectFromCookie:
            if login is None:
                response.setCookie(self.CAS_REDIRECT_URL, request.URL0, path="/")
            else:
                org_url = request.URL0
                if hasattr(request, "URL2"):
                    _url = request.URL2
                elif hasattr(request, "URL1"):
                    _url = request.URL1
                else:
                    _url = org_url
                response.setCookie(self.CAS_REDIRECT_URL, _url, path="/")

        # Redirect to CAS login URL.
        login_url = self.get_login_url()
        if not login_url:
            return False
        response.redirect(login_url, lock=True)
        return True

    # ICredentialsResetPlugin
    security.declarePrivate("resetCredentials")

    def resetCredentials(self, request, response):
        """Scribble as appropriate."""
        session = request.SESSION
        session.clear()
        try:
            self.logout_callback()
        except Exception as e:
            log.error(f"logout_callback Error in resetCredentials, {e}")
        settings = self.get_setting_data()
        if settings["cas_server"]:
            return response.redirect(self.get_logout_url(), lock=True)

    def logout_callback(self):
        sdm = getattr(self, "session_data_manager", None)  # Zope default session
        if sdm is not None:
            try:
                req = self.REQUEST
                alsoProvides(req, IDisableCSRFProtection)
            except AttributeError as e:
                log.error(f"IDisableCSRFProtection Error in logout_callback, {e}")
            session = sdm.getSessionData(create=0)
            if session is not None:
                try:
                    session.invalidate()
                except AttributeError:
                    session.clear()
                    self.REQUEST.response.expireCookie("__ac")
                except Exception:
                    pass

    def get_login_url(self):
        settings = self.get_setting_data()
        cas_server = settings["cas_server"]
        if not cas_server:
            return None
        cas_route = settings["cas_route"]
        cas_login_path = cas_route + "/login"
        service_url = settings["service_url"]
        login_url = create_cas_login_url(
            cas_server, cas_login_path, service_url, renew=None, gateway=None
        )
        return login_url

    def get_logout_url(self):
        settings = self.get_setting_data()
        cas_server = settings["cas_server"]
        if not cas_server:
            return None
        cas_route = settings["cas_route"]
        cas_logout_path = cas_route + "/logout"
        service_url = settings["service_url"]
        logout_url = create_cas_logout_url(cas_server, cas_logout_path, service_url)
        return logout_url

    def get_assertion(self, session):
        """
        :param session:
        :return: assertion principal
        """
        assertion = None
        if session:
            session_value = session.get(self.CAS_ASSERTION)
            if isinstance(session_value, dict):
                assertion = session_value
        return assertion

    def validate_service_ticket(self, ticket):
        log.debug("validating token {0}".format(ticket))
        settings = self.get_setting_data()
        cas_server = settings["cas_server"]
        if not cas_server:
            return None
        cas_route = settings["cas_route"]
        validate_path = cas_route + "/serviceValidate"
        service_url = settings["service_url"]
        validate_url = create_cas_validate_url(
            cas_server, validate_path, service_url, ticket
        )
        log.debug("Making GET request to {0}".format(validate_url))

        socket.setdefaulttimeout(5)
        try:
            response = urlopen(validate_url)
        except HTTPError as e:
            log.warning("HTTPError for CAS server: %s, Error: %s" % (validate_url, e))
            return None
        except URLError as e:
            log.warning("URLError for CAS server: %s, Error: %s" % (validate_url, e))
            return None
        except Exception as e:
            log.warning("Exception for Request: %s, Error: %s" % (validate_url, e))
            return None

        try:
            if six.PY2:
                xmldump = response.read().strip().decode("utf8", "ignore")
            else:
                xmldump = response.read().strip()
            xml_from_dict = parse(xmldump)
            is_valid = (
                True
                if "cas:authenticationSuccess" in xml_from_dict["cas:serviceResponse"]
                else False
            )
        except ValueError:
            log.error("CAS returned unexpected result")
            return None

        if is_valid:
            log.debug("valid")
            xml_from_dict = xml_from_dict["cas:serviceResponse"][
                "cas:authenticationSuccess"
            ]
            username = xml_from_dict["cas:user"]
            principal = {"user_id": username}
            if "cas:attributes" in xml_from_dict:
                attributes = xml_from_dict["cas:attributes"]
                if "cas:memberOf" in attributes:
                    attributes["cas:memberOf"] = (
                        attributes["cas:memberOf"].lstrip("[").rstrip("]").split(",")
                    )
                    for group_number in range(0, len(attributes["cas:memberOf"])):
                        attributes["cas:memberOf"][group_number] = (
                            attributes["cas:memberOf"][group_number]
                            .lstrip(" ")
                            .rstrip(" ")
                        )
                principal["attributes"] = attributes
            return principal
        else:
            log.debug("invalid ticket")
            return None

    @ram.cache(cache_key)
    def get_setting_data(self):
        registry = getUtility(IRegistry)
        settings = registry.forInterface(ICasSettingControlPanel)
        cas_server = settings.cas_server
        cas_route = settings.cas_route
        cas_after_login = settings.cas_after_login
        site = getSite()
        service_url = site.absolute_url()
        return {
            "cas_server": cas_server,
            "cas_route": cas_route,
            "cas_after_login": cas_after_login,
            "service_url": service_url,
        }


classImplements(CasclientHelper, ICasclientHelper)
InitializeClass(CasclientHelper)

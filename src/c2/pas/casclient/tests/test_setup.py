# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from c2.pas.casclient.testing import C2_PAS_CASCLIENT_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that c2.pas.casclient is properly installed."""

    layer = C2_PAS_CASCLIENT_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if c2.pas.casclient is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'c2.pas.casclient'))

    def test_browserlayer(self):
        """Test that IC2PasCasclientLayer is registered."""
        from c2.pas.casclient.interfaces import (
            IC2PasCasclientLayer)
        from plone.browserlayer import utils
        self.assertIn(
            IC2PasCasclientLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = C2_PAS_CASCLIENT_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer.uninstallProducts(['c2.pas.casclient'])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if c2.pas.casclient is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'c2.pas.casclient'))

    def test_browserlayer_removed(self):
        """Test that IC2PasCasclientLayer is removed."""
        from c2.pas.casclient.interfaces import \
            IC2PasCasclientLayer
        from plone.browserlayer import utils
        self.assertNotIn(
            IC2PasCasclientLayer,
            utils.registered_layers())

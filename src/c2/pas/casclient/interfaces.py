# -*- coding: utf-8 -*-
from Products.PluggableAuthService import interfaces


class ICasclientHelper(  # -*- implemented plugins -*-
    interfaces.plugins.IAuthenticationPlugin,
    interfaces.plugins.IExtractionPlugin,
    interfaces.plugins.IChallengePlugin,
    interfaces.plugins.ICredentialsResetPlugin
):
    """interface for IpbaseloginHelper."""

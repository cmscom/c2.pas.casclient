# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import c2.pas.casclient


class C2PasCasclientLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.app.dexterity
        self.loadZCML(package=plone.app.dexterity)
        self.loadZCML(package=c2.pas.casclient)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'c2.pas.casclient:default')


C2_PAS_CASCLIENT_FIXTURE = C2PasCasclientLayer()


C2_PAS_CASCLIENT_INTEGRATION_TESTING = IntegrationTesting(
    bases=(C2_PAS_CASCLIENT_FIXTURE,),
    name='C2PasCasclientLayer:IntegrationTesting',
)


C2_PAS_CASCLIENT_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(C2_PAS_CASCLIENT_FIXTURE,),
    name='C2PasCasclientLayer:FunctionalTesting',
)


C2_PAS_CASCLIENT_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        C2_PAS_CASCLIENT_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='C2PasCasclientLayer:AcceptanceTesting',
)

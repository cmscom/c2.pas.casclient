from zope import schema
from zope.interface import Interface

from c2.pas.casclient import C2PasCasclientMessageFactory as _


class ICasSettingControlPanel(Interface):
    """CAS Client setting interface
    """
    cas_server = schema.TextLine(
        title=u"CAS Server",
        description=u"URL of CAS: 'http://sso.pdx.edu'",
        required=False,
    )

    cas_route = schema.TextLine(
        title=u"CAS route",
        description=u"route path of CAS: '/root'",
        required=False,
    )

    cas_after_login = schema.TextLine(
        title=u"CAS after login (CallbackUrlPrefix)",
        description=u"Endpoint to go to after successful login: '/acl_users/PAS_ID'",
        required=False,
    )

from plone.app.registry.browser import controlpanel

from c2.pas.casclient.controlpanel.interfaces import ICasSettingControlPanel
from c2.pas.casclient import C2PasCasclientMessageFactory as _


class CasSettingEditForm(controlpanel.RegistryEditForm):

    schema = ICasSettingControlPanel
    label = _(u"CAS Client settings")


class CasSettingControlPanel(controlpanel.ControlPanelFormWrapper):
    form = CasSettingEditForm

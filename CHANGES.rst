Changelog
=========


1.0a2 (unreleased)
------------------

- Support Python 3 [terapyon]
- Support memcached for loggedout [terapyon]

1.0a1 (unreleased)
------------------

- Initial release.
  [terapyon]
